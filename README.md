# Umers Bookmarklets

This is a collection of bookmarklets that I find useful. The contents of each `dist/*.js` file can be saved as a bookmark prefixed with `javascript:`.

## open-local-interaction-details

Opens a new tab to the current route in the locally running interaction details ui wrapper app.

## open-local-web-directory

Opens a new tab to the ccurrent route in the locally running web-directory.

## open-local-quality-form

Opens the current quality form in the locally running quality forms app.

## copy-pc-auth

This copies the `pc_auth` from the site to the clipboard. Useful for getting the auth from web-directory.

## paste-pc-auth

This pastes the contents of the clipboard to the sites local storage as `pc_auth-localhost-dca`. This is
useful for setting the auth for locally running sites such as interaction-details-ui and quality-forms.
You will want to make sure you are at the correct port for the site, and not at some wrapper (8001 vs 3000).
