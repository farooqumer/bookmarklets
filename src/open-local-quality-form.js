(function () {
  const hash = window.location.hash;
  const editorRegEx = /(?<formType>evaluationForms|surveyForms)\/(?<formId>.*)/;
  const sidePanelRegEx = /interactions\/(?<interactionId>[^/]*)\/.*(?<formType>evaluationId|surveyId)=(?<formId>[^&]*)/;
  const tabRegEx = /interactions\/(?<interactionId>[^/]*)\/(?<formType>evaluations|surveys)\/(?<formId>.*)/;
  let newHash;

  if (editorRegEx.test(hash)) {
    const { formType, formId } = editorRegEx.exec(hash).groups;
    newHash = `${formType}/${formId}`;
  } else if (sidePanelRegEx.test(hash)) {
    const { interactionId, formType, formId } = sidePanelRegEx.exec(
      hash
    ).groups;
    newHash = `admin/interactions/${interactionId}/${formType.replace(
      "Id",
      "s"
    )}/${formId}`;
  } else if (tabRegEx.test(hash)) {
    const { interactionId, formType, formId } = tabRegEx.exec(hash).groups;
    newHash = `admin/interactions/${interactionId}/${formType}/${formId}`;
  }

  if (newHash) {
    window.open(`https://localhost:8002/#/${newHash}`);
  }
})();
